package com.silvano.wallbreaker.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.silvano.wallbreaker.entity.shots.DiagonalShot;
import com.silvano.wallbreaker.entity.shots.HorizontalShot;
import com.silvano.wallbreaker.entity.shots.SimpleShot;
import com.silvano.wallbreaker.utils.GameConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by silvano.filho on 02/06/2016.
 */
public class Spaceship extends BaseEntity {

    private static final int MAX_FIRE_DELAY = 20;
    private static final int MAX_SUPER_FIRE_DELAY = 100;

    // Texture and animation related
    private static final String SPACESHIP_CENTER_TEXTURE_1 = "Spaceship-center1";
    private static final String SPACESHIP_CENTER_TEXTURE_2 = "Spaceship-center2";
    private static final String SPACESHIP_CENTER_TEXTURE_3 = "Spaceship-center3";
    private static final String SPACESHIP_LEFT_TEXTURE_1 = "Spaceship-left1";
    private static final String SPACESHIP_LEFT_TEXTURE_2 = "Spaceship-left2";
    private static final String SPACESHIP_LEFT_TEXTURE_3 = "Spaceship-left3";
    private static final String SPACESHIP_RIGHT_TEXTURE_1 = "Spaceship-right1";
    private static final String SPACESHIP_RIGHT_TEXTURE_2 = "Spaceship-right2";
    private static final String SPACESHIP_RIGHT_TEXTURE_3 = "Spaceship-right3";
    private static final String SIMPLE_SHOT_TEXTURE = "SimpleShot";

    private enum SpaceshipAnimationEnum {
        CENTER,
        LEFT,
        RIGHT
    }

    private TextureRegion shotRegion;
    private Animation previousAnimation;
    private Animation animationCenter;
    private Animation animationLeft;
    private Animation animationRight;
    // End of textures and animation related variables

    // START OF SHOTS POOLS
    private final Pool<SimpleShot> simpleShotsPool = new Pool<SimpleShot>() {
        @Override
        protected SimpleShot newObject() {
            return new SimpleShot(position, SimpleShot.UP_SHOT);
        }
    };

    private final Pool<HorizontalShot> horizontalShotsPool = new Pool<HorizontalShot>() {
        @Override
        protected HorizontalShot newObject() {
            return new HorizontalShot(position, HorizontalShot.LEFT_SHOT);
        }
    };

    private final Pool<DiagonalShot> diagonalShotsPool = new Pool<DiagonalShot>() {
        @Override
        protected DiagonalShot newObject() {
            return new DiagonalShot(position, DiagonalShot.LEFT_SHOT);
        }
    };
    // END OF SHOTS POOLS

    private List<SimpleShot> shots;
    private List<SuperShot> superShots;
    private int shieldLevel;
    private int fireDelay;
    private int superShotsDelay;

    private boolean isIdle;
    private boolean isStopped;
    private boolean isAlive;

    public Spaceship(Vector2 position, int width, int height) {
        super(position, width, height);

        bound = new Rectangle(position.cpy().x, position.cpy().y, width / 2, height / 2);
        calculateBoundPosition();

        shots = new ArrayList<SimpleShot>();
        superShots = new ArrayList<SuperShot>();
        isIdle = true;
        isStopped = false;
        isAlive = true;
        shieldLevel = 0;
        fireDelay = 0;
        superShotsDelay = 0;
        previousAnimation = null;
    }

    public int getShieldLevel() {
        return shieldLevel;
    }

    public void setShieldLevel(int shieldLevel) {
        this.shieldLevel = shieldLevel;
    }

    public List<SimpleShot> getShots() {
        return shots;
    }

    public void removeShots(List<SimpleShot> shots) {
        this.shots.removeAll(shots);
        freeShotFromPool(shots);
    }

    private void freeShotFromPool(List<SimpleShot> shots) {
        for (SimpleShot shot : shots) {
            if (shot instanceof DiagonalShot) {
                diagonalShotsPool.free((DiagonalShot) shot);
            } else if (shot instanceof HorizontalShot) {
                horizontalShotsPool.free((HorizontalShot) shot);
            } else {
                simpleShotsPool.free(shot);
            }
        }
    }

    public List<SuperShot> getsuperShots() {
        return superShots;
    }

    public void setsuperShots(List<SuperShot> superShots) {
        this.superShots = superShots;
    }

    public void removesuperShots(List<SuperShot> superShots) {
        this.superShots.removeAll(superShots);
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setIsIdle(boolean isIdle) {
        this.isIdle = isIdle;
    }

    private void calculateBoundPosition() {
        float boundX = position.cpy().x;
        float boundY = position.cpy().y;
        boundX += width / 4;
        boundY += height / 4;
        bound.setPosition(boundX, boundY);
    }

    public Vector2 getCenter() {
        Vector2 center = new Vector2();
        center.x = position.cpy().x + width / 2;
        center.y = position.cpy().y + height / 2;

        return center;
    }

    public void upgrade(UpgradeType type) {
        switch (type) {
            case SHIELD:
                if (shieldLevel < 10)
                    shieldLevel++;
                break;
            default:
                break;
        }
    }

    public void stop() {
        speed = Vector2.Zero;
        isStopped = true;
    }

    public void ready(Vector2 starPos) {
        this.position = starPos;
        calculateBoundPosition();
        isIdle = true;
        freeShotFromPool(shots);
        shots.clear();
        superShots.clear();
        isAlive = true;
        shieldLevel = 0;
    }

    public void restart() {
        isStopped = false;
    }

    public void update(float delta) {
        if (!isStopped) {
            position.add(speed);

            // Keep the ship on the screen
            if (position.y > (GameConstants.SCREEN_HEIGHT - height))
                position.y = (GameConstants.SCREEN_HEIGHT - height);
            else if (position.y < GameConstants.START_GAMEPLAY_SCREEN)
                position.y = GameConstants.START_GAMEPLAY_SCREEN;
            if (position.x > (GameConstants.SCREEN_WIDTH - width) - 2)
                position.x = (GameConstants.SCREEN_WIDTH - width) - 2;
            else if (position.x < 0)
                position.x = 0;

            calculateBoundPosition();

            // update fire
            if (fireDelay > 0)
                fireDelay--;
            else if (fireDelay == 0) {
                fireDelay = MAX_FIRE_DELAY;
                int firePosX = (int) position.x + width / 2;
                int firePosY = (int) position.y - 14;
                //AssetManager.lazer.play(0.2f);

                Vector2 firePosition = new Vector2(firePosX, firePosY);
                initializeShots(firePosition);
            }

            for (SimpleShot shot : shots) {
                shot.update();
            }

            if (superShotsDelay > 0)
                superShotsDelay--;
            else if (superShotsDelay == 0){
                superShotsDelay = MAX_SUPER_FIRE_DELAY;
                int firePosX = (int) position.x + width / 2;
                int firePosY = (int) position.y - 14;
                superShots.add(new SuperShot(new Vector2(firePosX, firePosY)));
            }

            for (SuperShot superShot : superShots) {
                superShot.update(delta);
            }
        }
    }

    public void initializeShots(Vector2 firePosition) {
        // Simple (Vertical) Shots
        SimpleShot upShot = simpleShotsPool.obtain();
        upShot.init(firePosition.cpy(), SimpleShot.UP_SHOT, shotRegion);
        /*SimpleShot downShot = simpleShotsPool.obtain();
        downShot.init(firePosition.cpy(), SimpleShot.DOWN_SHOT, shotRegion);
        // Horizontal Shots
        HorizontalShot leftShot = horizontalShotsPool.obtain();
        leftShot.init(firePosition.cpy(), HorizontalShot.LEFT_SHOT, shotRegion);
        HorizontalShot rightShot = horizontalShotsPool.obtain();
        rightShot.init(firePosition.cpy(), HorizontalShot.RIGHT_SHOT, shotRegion);*/
        // Diagonal Shots
        DiagonalShot diagLeftShot = diagonalShotsPool.obtain();
        diagLeftShot.init(firePosition.cpy(), DiagonalShot.LEFT_SHOT, shotRegion);
        DiagonalShot diagRightShot = diagonalShotsPool.obtain();
        diagRightShot.init(firePosition.cpy(), DiagonalShot.RIGHT_SHOT, shotRegion);
        /*DiagonalShot diagBackLeftShot = diagonalShotsPool.obtain();
        diagBackLeftShot.init(firePosition.cpy(), DiagonalShot.BACK_LEFT_SHOT, shotRegion);
        DiagonalShot diagBackRightShot = diagonalShotsPool.obtain();
        diagBackRightShot.init(firePosition.cpy(), DiagonalShot.BACK_RIGHT_SHOT, shotRegion);*/

        shots.add(upShot);
        shots.add(diagLeftShot);
        shots.add(diagRightShot);
        /*shots.add(downShot);
        shots.add(diagBackLeftShot);
        shots.add(diagBackRightShot);
        shots.add(leftShot);
        shots.add(rightShot);*/
    }

    public void render(TextureAtlas atlas, SpriteBatch batch, float delta) {

        TextureRegion region;
        Animation currentAnimation;

        // Fix incorrect left/center/right animation
        if (isIdle) {
            currentAnimation = animationCenter;
        } else {
            if (speed.x > 0) {
                currentAnimation = animationRight;
            } else if (speed.x < 0) {
                currentAnimation = animationLeft;
            } else {
                currentAnimation = previousAnimation;
            }
        }

        if (previousAnimation != currentAnimation) {
            previousAnimation = currentAnimation;
        }

        region = currentAnimation.getKeyFrame(delta);
        batch.draw(region, position.x, position.y, region.getRegionWidth(), region.getRegionHeight());

        for (SimpleShot shot : shots) {
            shot.render(batch, atlas);
        }

        for (SuperShot superShot : superShots) {
            superShot.render(batch,atlas);
        }
    }

    @Override
    public void initTextures(TextureAtlas atlas) {
        animationCenter = getSpaceshipAnimation(atlas, SpaceshipAnimationEnum.CENTER);
        animationLeft = getSpaceshipAnimation(atlas, SpaceshipAnimationEnum.LEFT);
        animationRight = getSpaceshipAnimation(atlas, SpaceshipAnimationEnum.RIGHT);
        shotRegion = flipTexture(atlas, SIMPLE_SHOT_TEXTURE);
    }

    private Animation getSpaceshipAnimation(TextureAtlas atlas, SpaceshipAnimationEnum positionAnim) {
        // array containing the textures
        TextureRegion[] shipsAnim = new TextureRegion[3];
        if (positionAnim == SpaceshipAnimationEnum.CENTER) {
            shipsAnim[0] = atlas.findRegion(SPACESHIP_CENTER_TEXTURE_1);
            shipsAnim[1] = atlas.findRegion(SPACESHIP_CENTER_TEXTURE_2);
            shipsAnim[2] = atlas.findRegion(SPACESHIP_CENTER_TEXTURE_3);
        } else if (positionAnim == SpaceshipAnimationEnum.LEFT) {
            shipsAnim[0] = atlas.findRegion(SPACESHIP_LEFT_TEXTURE_1);
            shipsAnim[1] = atlas.findRegion(SPACESHIP_LEFT_TEXTURE_2);
            shipsAnim[2] = atlas.findRegion(SPACESHIP_LEFT_TEXTURE_3);
        } else if (positionAnim == SpaceshipAnimationEnum.RIGHT) {
            shipsAnim[0] = atlas.findRegion(SPACESHIP_RIGHT_TEXTURE_1);
            shipsAnim[1] = atlas.findRegion(SPACESHIP_RIGHT_TEXTURE_2);
            shipsAnim[2] = atlas.findRegion(SPACESHIP_RIGHT_TEXTURE_3);
        }

        // flip the image to follow the coordinates starting from top
        if (!shipsAnim[0].isFlipY()) {
            shipsAnim[0].flip(false, true);
            shipsAnim[1].flip(false, true);
            shipsAnim[2].flip(false, true);
        }

        Animation animation = new Animation(0.06f, shipsAnim);
        animation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
        return animation;
    }

    public boolean collides(Rectangle rect) {
        return Intersector.overlaps(rect, bound);
    }

    public boolean collides(Circle circle) {
        return Intersector.overlaps(circle, bound);
    }

    public void collided() {
        if (shieldLevel == 0)
            isAlive = false;
        else
            shieldLevel--;
    }

    public void collided(int lifeLost) {
        if (shieldLevel <= lifeLost) {
            isAlive = false;
            shieldLevel = 0;
        } else
            shieldLevel -= shieldLevel - lifeLost;
    }

}
