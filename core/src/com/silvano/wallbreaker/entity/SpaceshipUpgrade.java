package com.silvano.wallbreaker.entity;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by silvano.filho on 06/06/2016.
 */
public class SpaceshipUpgrade extends BaseEntity {

    private static final String UPGRADE_TEXTURE = "upgrade";

    private UpgradeType type;

    public SpaceshipUpgrade(Vector2 position) {
        super(position, 8, 10);

        speed = new Vector2(0, 4);
        bound = new Rectangle(position.x, position.y, width, height);
        // TODO implementar random type
        type = UpgradeType.SHIELD;
    }

    public UpgradeType getType() {
        return type;
    }

    public void pause() {
        speed = Vector2.Zero;
    }

    public void restart() {
        speed = new Vector2(0, 2);
    }

    public void update(float delta) {
        position.add(speed);
        bound.setPosition(position);
    }

    public void render(SpriteBatch batch, TextureAtlas atlas) {
        if (region == null) {
            // TODO we have to remove this condition, after using the 'pool' stuff
            initTextures(atlas);
        }
        batch.draw(region, position.x, position.y, width / 2, height / 2, width, height, 2, 2, 0);
    }

    @Override
    public void initTextures(TextureAtlas atlas) {
        region = flipTexture(atlas, UPGRADE_TEXTURE);
    }

    @Override
    public boolean collides(Rectangle rect) {
        return Intersector.overlaps(rect, bound);
    }

    @Override
    public boolean collides(Circle circle) {
        return Intersector.overlaps(circle, bound);
    }
}
