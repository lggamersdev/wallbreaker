package com.silvano.wallbreaker.entity;

import com.badlogic.gdx.math.Vector2;
import com.silvano.wallbreaker.utils.GameConstants;

/**
 * Base class for tiles and turrets
 */
public abstract class Scrollable extends BaseEntity {

    protected final int LEVEL_MAX = 3;

    protected boolean isScrolledDown;
    protected boolean isStopped;
    protected int level;
    protected float scrollSpeed;

    public Scrollable(Vector2 position, int width, int height, float scrollSpeed) {
        super(position, width, height);

        speed = new Vector2(0, scrollSpeed);
        isScrolledDown = false;
        this.scrollSpeed = scrollSpeed;
        isStopped = false;
    }

    public int getLevel() {
        return level;
    }

    public boolean isScrolledDown() {
        return isScrolledDown;
    }

    public void levelUp() {
        if (level < LEVEL_MAX) {
            level++;
        }
    }

    public void resetLevel() {
        level = 0;
    }

    public void stop() {
        speed = Vector2.Zero;
        isStopped = true;
    }

    public void restart() {
        speed = new Vector2(0, scrollSpeed);
        isStopped = false;
    }

    public void reset(float newPosY) {
        position.y = newPosY;
        isScrolledDown = false;
    }

    public void update(float delta) {
        position.add(speed.cpy().scl(delta));

        if (isBelowScreen())
            isScrolledDown = true;
    }
}
