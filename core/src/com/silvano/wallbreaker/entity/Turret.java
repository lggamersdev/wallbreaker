package com.silvano.wallbreaker.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.silvano.wallbreaker.entity.shots.EnemyShot;
import com.silvano.wallbreaker.utils.GameConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by silvano.filho on 06/06/2016.
 */
public class Turret extends Scrollable {

    private static final String TURRET_TEXTURE = "Turret";
    private static final int START_LIFE = 5;

    private Vector2 shipPosition;
    private boolean isCollidable;
    private int life;
    private float rotation;
    private List<EnemyShot> shots;
    private float fireDelay;

    private Pool<EnemyShot> shotsPool = new Pool<EnemyShot>() {
        @Override
        protected EnemyShot newObject() {
            return new EnemyShot(position, shipPosition, scrollSpeed);
        }
    };

    public Turret(Vector2 position, float scrollSpeed) {
        super(position, GameConstants.TURRET_WIDTH, GameConstants.TURRET_HEIGHT, scrollSpeed);

        bound = new Rectangle(position.cpy().x-5, position.cpy().y, 30, 30);
        shipPosition = new Vector2(0, position.y - 1);
        isCollidable = true;
        level = 0;
        life = START_LIFE;
        rotation = 0;
        shots = new ArrayList<EnemyShot>();
        fireDelay = 0;
    }

    public Turret(int x, int y, float scrollSpeed) {
        this(new Vector2(x,y), scrollSpeed);
    }

    public boolean isCollidable() {
        return isCollidable;
    }

    public boolean isDead() {
        return life == 0;
    }

    public void setCollidable(boolean isCollidable) {
        this.isCollidable = isCollidable;
    }

    public void setShipPosition(Vector2 shipPosition) {
        this.shipPosition = shipPosition.cpy();
    }

    public void freeShotsFromPool(List<EnemyShot> shots) {
        for (EnemyShot shot : shots) {
            shotsPool.free(shot);
        }
    }

    public void reset(float newPosY, float newPosX) {
        super.reset(newPosY);

        position.x = newPosX;

        life = START_LIFE + (level * 2);
        isCollidable = true;
        freeShotsFromPool(shots);
        shots.clear();
    }

    @Override
    public void reset(float newPosY) {
        super.reset(newPosY);

        life = START_LIFE + (level * 2);
        isCollidable = true;
        shots.clear();
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        Vector2 boundPos = new Vector2(position.cpy().x-5, position.cpy().y);
        bound.setPosition(boundPos);

        if (isCollidable && !isStopped && shipPosition.y > position.cpy().y) {
            rotation = shipPosition.cpy().sub(position.cpy()).angle();
            rotation -= 90;

            if (fireDelay > 0)
                fireDelay--;
            else if (fireDelay == 0 && onGameplayScreen()) {
                fireDelay = 50;
                Vector2 centerPos = new Vector2(position.cpy().x + width / 2, position.cpy().y
                        + height / 2);
                float rotRadians = (float) Math.toRadians(rotation);
                float firePosX = centerPos.x - (float) (Math.cos(rotRadians));
                float firePosY = centerPos.y - (float) (Math.sin(rotRadians));

                EnemyShot shot = shotsPool.obtain();
                shot.init(new Vector2(firePosX, firePosY), shipPosition.cpy(), scrollSpeed);
                shots.add(shot);
            }
        }

        List<EnemyShot> outOfBoundsShots = new ArrayList<EnemyShot>();
        for (EnemyShot shot : shots) {
            shot.update(delta);
            if (shot.getPosition().y > GameConstants.SCREEN_HEIGHT)
                outOfBoundsShots.add(shot);
        }

        freeShotsFromPool(outOfBoundsShots);
        shots.removeAll(outOfBoundsShots);
    }

    public void render(SpriteBatch batch, TextureAtlas atlas) {
        if (onGameplayScreen()) {
            for (EnemyShot shot : shots) {
                shot.render(batch, atlas);
            }
        }

        if (isCollidable) {
            if (region == null) {
                // TODO we have to remove this condition, after using the 'pool' stuff
                initTextures(atlas);
            }

            Color c = batch.getColor();
            if(life <= START_LIFE / 3) {
                batch.setColor(1,0,0,1);
            } else if (life <= START_LIFE * 2 / 3) {
                batch.setColor(1,0.5f,0.5f,1);
            }
            batch.draw(region, position.x, position.y, width / 2, height / 2, width, height, 1.5f, 1.5f, rotation);
            batch.setColor(c);
        }
    }

    @Override
    public void initTextures(TextureAtlas atlas) {
        region = flipTexture(atlas, TURRET_TEXTURE);
    }

    public boolean collides(Rectangle rect) {
        boolean collided = false;
        if (isCollidable && Intersector.overlaps(bound, rect)) {
            collided();
            collided = true;
        } else {
            List<EnemyShot> removableShots = new ArrayList<EnemyShot>();
            for (EnemyShot shot : shots) {
                if (shot.collides(rect)) {
                    removableShots.add(shot);
                    collided = true;
                }
            }
            shots.removeAll(removableShots);
        }
        return collided;
    }

    public boolean collides(Circle circle) {
        boolean collided = false;
        if (isCollidable && Intersector.overlaps(circle, bound)) {
            collided();
            collided = true;
        }
        return collided;
    }

    public void collided() {
        life -= 1;
        if (life <= 0)
            isCollidable = false;
    }
}
