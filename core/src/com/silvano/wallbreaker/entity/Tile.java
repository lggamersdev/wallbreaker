package com.silvano.wallbreaker.entity;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.silvano.wallbreaker.utils.GameConstants;

/**
 * Created by silvano.filho on 06/06/2016.
 */
public class Tile extends Scrollable {

    private static final String TILE_TEXTURE = "Wall";
    private static final int START_LIFE = 3;

    private boolean isCollidable;
    private int tileLife;
    private int currentLevel;

    public Tile(Vector2 position, float scrollSpeed) {
        super(position, GameConstants.TILE_WIDTH_HEIGHT, GameConstants.TILE_WIDTH_HEIGHT,
                scrollSpeed);

        bound = new Rectangle(position.x, position.y, width, height);
        isCollidable = true;
        level = 0;
        tileLife = START_LIFE;
        currentLevel = level;
    }

    public Tile(int x, int y, float scrollSpeed) {
        this(new Vector2(x,y), scrollSpeed);
    }

    public boolean isCollidable() {
        return isCollidable;
    }

    public int getTail() {
        return (int) position.cpy().y;
    }

    public boolean isDead() {
        return tileLife == 0;
    }

    @Override
    public void reset(float newPosY) {
        super.reset(newPosY);

        tileLife = START_LIFE + (level * 2);
        isCollidable = true;
        currentLevel = level;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        bound.setPosition(position);
    }

    public void render(SpriteBatch batch, TextureAtlas atlas) {
        if (isCollidable) {
            int levelLife = START_LIFE + (currentLevel *2);
            Color c = batch.getColor();
            if (tileLife <= levelLife-2*(currentLevel +1))
                batch.setColor(1,0,0,1);
            else if (tileLife <= levelLife-(currentLevel +1))
                batch.setColor(1,0.5f,0.5f,1);

            batch.draw(region, position.x, position.y, width, height);
            batch.setColor(c);
        }
    }

    @Override
    public void initTextures(TextureAtlas atlas) {
        region = flipTexture(atlas, TILE_TEXTURE);
    }

    public boolean collides(Rectangle rect) {
        boolean isShip = false;
        if (rect.getWidth() == (GameConstants.SPACESHIP_WIDTH/2))
            isShip = true;
        if (Intersector.overlaps(bound, rect)) {
            collided(isShip);
            return true;
        }
        return false;
    }

    public boolean collides(Circle circle) {
        if (Intersector.overlaps(circle, bound)) {
            collided(false);
            return true;
        }
        return false;
    }

    private void collided(boolean isShip) {
        if (isCollidable) {
            tileLife -= 1;
            if (tileLife <= 0 || isShip)
                isCollidable = false;
        }
    }
}
