package com.silvano.wallbreaker.entity;

/**
 * Created by silvano.filho on 03/06/2016.
 */
public enum UpgradeType {
    // TODO implement other types of upgrades
    SHIELD,
    EXTRA_SHOT,
    SPEED_SHOT,
    ROBOT_HELPER
}
