package com.silvano.wallbreaker.entity;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.silvano.wallbreaker.utils.GameConstants;

/**
 * Created by silvano.filho on 02/06/2016.
 */
public abstract class BaseEntity {

    protected int width;
    protected int height;
    protected Vector2 position;
    protected Vector2 speed;

    protected Rectangle bound;

    protected TextureRegion region;

    public BaseEntity(Vector2 position, int width, int height) {
        this(position, width, height, Vector2.Zero);
    }

    public BaseEntity(Vector2 position, int width, int height, Vector2 speed) {
        this.width = width;
        this.height = height;
        this.position = position;
        this.speed = speed;
    }

    // Getters and Setters
    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Vector2 getSpeed() {
        return speed;
    }

    public void setSpeed(Vector2 speed) {
        this.speed = speed;
    }

    public Rectangle getBound() {
        return bound;
    }

    public void setBound(Rectangle bound) {
        this.bound = bound;
    }

    public TextureRegion getRegion() {
        return region;
    }

    public void setRegion(TextureRegion region) {
        this.region = region;
    }

    // verify if the object is still appearing on screen
    public boolean onGameplayScreen() {
        if (position.x > GameConstants.SCREEN_WIDTH || position.x < 0
                || position.y < GameConstants.START_GAMEPLAY_SCREEN
                || isBelowScreen())
            return false;
        return true;
    }

    // verify if the object is below of the bottom of the screen
    public boolean isBelowScreen() {
        if (position.y >  GameConstants.SCREEN_HEIGHT)
            return true;
        return false;
    }

    public abstract void initTextures(TextureAtlas atlas);

    // Used to flip sprites and images to the top left (0,0) orientation
    public TextureRegion flipTexture(TextureAtlas atlas, String texture) {
        TextureRegion region = atlas.findRegion(texture);
        if (!region.isFlipY())
            region.flip(false,true);

        return region;
    }

    // Collision methods are implemented on childs, since each has a different way to treat it
    public abstract boolean collides(Rectangle rect);
    public abstract boolean collides(Circle circle);
}
