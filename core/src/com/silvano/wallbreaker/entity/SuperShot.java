package com.silvano.wallbreaker.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by rodrigo.camara on 30/12/2016.
 */

public class SuperShot extends BaseEntity {


    private static final int SIMPLE_SHOT_WIDTH = 15;
    private static final int SIMPLE_SHOT_HEIGHT = 50;
    private static final float VERTICAL_SPEED = -10;

    private final String SUPER_SHOT_TEXTURE = "SimpleShot";

    public SuperShot(Vector2 position) {
        super(position, SIMPLE_SHOT_WIDTH, SIMPLE_SHOT_HEIGHT);

        speed = new Vector2(0, VERTICAL_SPEED);
        bound = new Rectangle(position.x, position.y, width, height);
    }

    public void update(float delta) {
        position.add(speed);
        bound.setPosition(position);
    }

    public void render(SpriteBatch sb, TextureAtlas atlas) {
        if (region == null) {
            // TODO we have to remove this condition, after using the 'pool' stuff
            initTextures(atlas);
        }
        if (onGameplayScreen()) {
            sb.draw(region, position.x, position.y, width / 4, height / 4, width / 2, height / 2, 2, 2, 0);
        }
    }

    @Override
    public void initTextures(TextureAtlas atlas) {
        region = flipTexture(atlas, SUPER_SHOT_TEXTURE);
    }

    public boolean collides(Rectangle rect) {
        return Intersector.overlaps(rect, bound);
    }

    public boolean collides(Circle circle) {
        return Intersector.overlaps(circle, bound);
    }
}
