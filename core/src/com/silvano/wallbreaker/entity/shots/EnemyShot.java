package com.silvano.wallbreaker.entity.shots;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.silvano.wallbreaker.entity.RoundEntity;

/**
 * The turret shots
 */
public class EnemyShot extends RoundEntity implements Pool.Poolable {

    private static final String ENEMY_SHOT_TEXTURE = "EnemyShot";
    private static final float SPEED_MAX = 4;

    private Vector2 target;
    private float scrollSpeed;
    private boolean isCollidable;

    public EnemyShot(Vector2 position, Vector2 targetPos, float scrollSpeed) {
        super(position, 8);
        init(position, targetPos, scrollSpeed);
    }

    public void init(Vector2 position, Vector2 targetPos, float scrollSpeed) {
        this.position = position;
        this.target = targetPos;
        verifySpeed();
        bound = new Circle(new Vector2(position.x + radius / 2, position.y + radius / 2), radius / 2);
        this.scrollSpeed = scrollSpeed;
        isCollidable = true;
    }

    public void verifySpeed() {
        int deltaX = (int) (target.cpy().x - position.cpy().x);
        int deltaY = (int) (target.cpy().y - position.cpy().y);

        speed = new Vector2(deltaX, deltaY).nor().scl(Math.min(position.dst(target), SPEED_MAX));
    }

    public void update(float delta) {
        // Fix the shot direction, so it won't give the impression of going up
        float speedY = scrollSpeed * delta;
        if (speed.y < speedY) {
            speed.y = speedY;
        }

        position.add(speed);
        bound.setPosition(position.x + radius / 2, position.y + radius / 2);
    }

    public void render(SpriteBatch batch, TextureAtlas atlas) {
        if (region == null) {
            // TODO we have to remove this condition, after using the 'pool' stuff
            initTextures(atlas);
        }
        if (!isBelowScreen() && isCollidable) {
            batch.draw(region, position.x, position.y, radius / 2, radius / 2, radius, radius, 2, 2, 0);
        }
    }

    @Override
    public void initTextures(TextureAtlas atlas) {
        region = flipTexture(atlas, ENEMY_SHOT_TEXTURE);
    }

    public boolean collides(Rectangle rect) {
        return Intersector.overlaps(bound, rect);
    }

    public boolean collides(Circle circle) {
        return Intersector.overlaps(circle, bound);
    }

    @Override
    public void reset() {
        isCollidable = false;
        speed = Vector2.Zero;
    }
}
