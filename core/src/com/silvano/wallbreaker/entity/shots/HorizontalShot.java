package com.silvano.wallbreaker.entity.shots;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.silvano.wallbreaker.entity.shots.SimpleShot;

/**
 * Created by silvano.filho on 02/06/2016.
 */
public class HorizontalShot extends SimpleShot {

    public static final int LEFT_SHOT = 0;
    public static final int RIGHT_SHOT = 1;

    private final String SIMPLE_SHOT_TEXTURE = "SimpleShot";

    public HorizontalShot(Vector2 position, int side) {
        super(position, side);
        init(position, side, null);
    }

    @Override
    protected Vector2 speedDirection() {
        return new Vector2((side == LEFT_SHOT) ? SPEED : -SPEED, 0);
    }

    public void render(SpriteBatch sb, TextureAtlas atlas) {
        if (region == null) {
            initTextures(atlas);
        }
        if (onGameplayScreen() && isCollidable) {
            sb.draw(region, position.x, position.y, radius / 4, radius / 4, radius / 2, radius / 2, 2, 2, getRotation());
        }
    }

    private int getRotation() {
        if(side == RIGHT_SHOT) {
            return 90;
        } else {
            return -90;
        }
    }

    public boolean collides(Rectangle rect) {
        return Intersector.overlaps(bound, rect);
    }

    public boolean collides(Circle circle) {
        return Intersector.overlaps(circle, bound);
    }
}
