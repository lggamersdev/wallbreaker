package com.silvano.wallbreaker.entity.shots;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import com.silvano.wallbreaker.entity.RoundEntity;

/**
 * Created by silvano.filho on 02/06/2016.
 */
public class SimpleShot extends RoundEntity implements Pool.Poolable {

    public static final int UP_SHOT = 0;
    public static final int DOWN_SHOT = 1;
    protected static final float SIMPLE_SHOT_RADIUS = 8;
    protected static final float SPEED = -5;

    private final String SIMPLE_SHOT_TEXTURE = "SimpleShot";
    protected int side;
    protected boolean isCollidable;

    public SimpleShot(Vector2 position, int side) {
        super(position, SIMPLE_SHOT_RADIUS, Vector2.Zero);
        init(position, side, null);
    }

    public int getSide() {
        return side;
    }

    public void init(Vector2 position, int side, TextureRegion region) {
        this.position = position.cpy();
        this.side = side;
        speed = speedDirection().cpy();
        bound = new Circle(position.cpy().x, position.cpy().y, SIMPLE_SHOT_RADIUS);
        isCollidable = true;
        this.region = region;
    }

    protected Vector2 speedDirection() {
        return new Vector2(0, (side == UP_SHOT) ? SPEED : -SPEED);
    }

    public void update() {
        if (isCollidable) {
            position.add(speed);
            bound.setPosition(position);
        }
    }

    public void render(SpriteBatch sb, TextureAtlas atlas) {
        if (region == null) {
            initTextures(atlas);
        }
        if (onGameplayScreen() && isCollidable) {
            sb.draw(region, position.x, position.y, radius / 4, radius / 4, radius / 2, radius / 2, 2, 2, getRotation());
        }
    }

    @Override
    public void initTextures(TextureAtlas atlas) {
        region = flipTexture(atlas, SIMPLE_SHOT_TEXTURE);
    }

    private int getRotation() {
        if(side == DOWN_SHOT) {
            return 180;
        } else {
            return 0;
        }
    }

    public boolean collides(Rectangle rect) {
        return Intersector.overlaps(bound, rect);
    }

    public boolean collides(Circle circle) {
        return Intersector.overlaps(circle, bound);
    }

    @Override
    public void reset() {
        isCollidable = false;
        speed = Vector2.Zero;
    }
}
