package com.silvano.wallbreaker.entity;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.silvano.wallbreaker.utils.GameConstants;

/**
 * Created by felipe.acerbi on 04/01/2017.
 */

public abstract class RoundEntity {

    protected float radius;
    protected Vector2 position;
    protected Vector2 speed;

    protected Circle bound;

    protected TextureRegion region;

    public RoundEntity(Vector2 position, float radius) {
        this(position, radius, Vector2.Zero);
    }

    public RoundEntity(Vector2 position, float radius, Vector2 speed) {
        this.radius = radius;
        this.position = position;
        this.speed = speed;
    }

    // Getters and Setters
    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void setSpeed(Vector2 speed) {
        this.speed = speed;
    }

    public Circle getBound() {
        return bound;
    }

    public void setBound(Circle bound) {
        this.bound = bound;
    }

    public TextureRegion getRegion() {
        return region;
    }

    public void setRegion(TextureRegion region) {
        this.region = region;
    }

    // verify if the object is still appearing on screen
    public boolean onGameplayScreen() {
        if (position.x > GameConstants.SCREEN_WIDTH || position.x < 0
                || position.y < GameConstants.START_GAMEPLAY_SCREEN
                || isBelowScreen())
            return false;
        return true;
    }

    // verify if the object is below of the bottom of the screen
    public boolean isBelowScreen() {
        if (position.y >  GameConstants.SCREEN_HEIGHT)
            return true;
        return false;
    }

    public abstract void initTextures(TextureAtlas atlas);

    // Used to flip sprites and images to the top left (0,0) orientation
    public TextureRegion flipTexture(TextureAtlas atlas, String texture) {
        TextureRegion region = atlas.findRegion(texture);
        if (!region.isFlipY())
            region.flip(false,true);

        return region;
    }

    // Collision methods are implemented on childs, since each has a different way to treat it
    public abstract boolean collides(Rectangle rect);
    public abstract boolean collides(Circle circle);

}
