package com.silvano.wallbreaker.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.silvano.wallbreaker.entity.Turret;
import com.silvano.wallbreaker.utils.GameConstants;
import com.silvano.wallbreaker.utils.LanguageUtils;
import com.silvano.wallbreaker.utils.TextUtils;

/**
 * Base renderer, this class calls each object to render themselves, since each follows their own rules.
 */
public class WorldRenderer {

    private static final String BACKGROUND_TEXTURE = "background";

    private GameWorld world;
    private TextureAtlas atlas;
    private BitmapFont font;
    private TextureRegion backgroundRegion;

    private SpriteBatch batch;
    private ShapeRenderer shape;
    private OrthographicCamera cam;

    private String pointsText;
    private String shieldText;
    private String scoreText;
    private String statusText;
    private String highscoreText;

    public WorldRenderer (GameWorld world, AssetManager assetManager, SpriteBatch batch, ShapeRenderer shape) {
        this.world = world;
        this.atlas = assetManager.get(GameConstants.TEXTURE_ATLAS, TextureAtlas.class);
        this.batch = batch;
        this.shape = shape;
        this.font = TextUtils.createFont();

        cam = new OrthographicCamera();
        cam.setToOrtho(true, GameConstants.SCREEN_WIDTH, GameConstants.SCREEN_HEIGHT);

        batch.setProjectionMatrix(cam.combined);
        shape.setProjectionMatrix(cam.combined);

        initTexts();
        initTextures();
    }

    private void initTexts() {
        pointsText = LanguageUtils.getLanguageBundle().get("inGameLevel");
        shieldText = LanguageUtils.getLanguageBundle().get("inGameShield");
        scoreText = LanguageUtils.getLanguageBundle().get("inGameScore");
        highscoreText =  LanguageUtils.getLanguageBundle().get("highScore");
        statusText = "";
    }

    private void initTextures() {
        backgroundRegion = atlas.findRegion(BACKGROUND_TEXTURE);
        if (!backgroundRegion.isFlipY()) {
            backgroundRegion.flip(false, true);
        }
        world.getShip().initTextures(atlas);
        world.getScrollHandler().getWall1().initTextures(atlas);
        world.getScrollHandler().getWall2().initTextures(atlas);
    }

    public void render(float delta) {
        // We draw a black background. This prevents flickering.
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        // render background
        batch.disableBlending();
        batch.draw(backgroundRegion, 0, GameConstants.START_GAMEPLAY_SCREEN,
                GameConstants.SCREEN_WIDTH, (GameConstants.SCREEN_HEIGHT-GameConstants.START_GAMEPLAY_SCREEN));
        batch.enableBlending();

        // render Spaceship, upgrades, walls and turrets
        world.getShip().render(atlas, batch, delta);
        if (world.getUpgrade() != null)
            world.getUpgrade().render(batch, atlas);

        for (Turret t : world.getScrollHandler().getTurrets()) {
            t.render(batch, atlas);
        }

        world.getScrollHandler().getWall1().render(batch, atlas);
        world.getScrollHandler().getWall2().render(batch, atlas);
        batch.end();

        // HUD
        // Paint the space reserved for the game HUD
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(Color.BLACK);
        shape.rect(0, 0, 450, 80);
        shape.end();

        // Game texts
        batch.begin();

        int level = world.getLevel();
        int score = world.getScore();
        int highScore = world.getHighScore();

        Vector2 centerOfPointText = TextUtils.getCenterOfText(font, pointsText + level);
        //Vector2 centerOfScoreText = TextUtils.getCenterOfText(font, scoreText + score);
        Vector2 centerOfShieldText = TextUtils.getCenterOfText(font, shieldText);
        Vector2 centerOfStatusText = Vector2.Zero;

        font.draw(batch, pointsText + level, 10, TextUtils.getWidthOrHeightFontPosition(GameConstants.START_GAMEPLAY_SCREEN, centerOfPointText.y));
        font.draw(batch, shieldText, GameConstants.SCREEN_WIDTH/2, TextUtils.getWidthOrHeightFontPosition(GameConstants.START_GAMEPLAY_SCREEN, centerOfShieldText.y));
        font.draw(batch, scoreText + score, 10, TextUtils.getBelowText(font, pointsText) + 40);
        font.draw(batch, highscoreText + highScore, GameConstants.SCREEN_WIDTH/2, TextUtils.getBelowText(font, pointsText) + 40);

        if (world.getState() == GameWorld.GameState.READY) {
            statusText = LanguageUtils.getLanguageBundle().get("inGameReady");
            centerOfStatusText = TextUtils.getCenterOfText(font, statusText);
        } else if (world.getState() == GameWorld.GameState.GAME_OVER) {
            statusText = LanguageUtils.getLanguageBundle().get("inGameOver");
            centerOfStatusText = TextUtils.getCenterOfText(font, statusText);
        } else if (world.getState() == GameWorld.GameState.PAUSE) {
            statusText = LanguageUtils.getLanguageBundle().get("inGamePause");
            centerOfStatusText = TextUtils.getCenterOfText(font, statusText);
        }

        if (!(world.getState() == GameWorld.GameState.RUNNING)) {
            // draw the game status
            float statusTextHeight = TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_HEIGHT, centerOfStatusText.y);
            font.draw(batch, statusText, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfStatusText.x), statusTextHeight);

            // draw the highscore
            Preferences pref = Gdx.app.getPreferences(GameConstants.WALLBREAKER_PREFS);
            int highscore = pref.getInteger(GameConstants.HIGHSCORE_PREF, 0);
            Vector2 centerOfHighscoreText = TextUtils.getCenterOfText(font, highscoreText + highscore);
            font.draw(batch, highscoreText + highscore, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfHighscoreText.x),
                   statusTextHeight-(centerOfHighscoreText.y*2));

            String shipPartsText = "Ship Parts: " + pref.getInteger(GameConstants.SHIP_PARTS, 0);
            Vector2 centerOfShipPartsText = TextUtils.getCenterOfText(font, shipPartsText);
            font.draw(batch, shipPartsText, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfShipPartsText.x),
                    statusTextHeight-(centerOfHighscoreText.y*2)-((centerOfShipPartsText.y*2)+ 20));

            String currentScore = scoreText + score;
            Vector2 centerOfCurrentScoreText = TextUtils.getCenterOfText(font, currentScore);
            font.draw(batch, currentScore, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfCurrentScoreText.x),
                    statusTextHeight-(centerOfHighscoreText.y*2)-(centerOfCurrentScoreText.y + 10));
        }

        batch.end();

        // calculate where the shield text ends
        float shieldWidth = GameConstants.SCREEN_WIDTH/2 + centerOfShieldText.x;

        // draw the rectangle that shows the shield level to the player
        shape.begin(ShapeRenderer.ShapeType.Line);
        shape.setColor(Color.WHITE);
        shape.rect(shieldWidth+10, 10, 100, 25);
        shape.end();

        // fill the rectangle above according to the shield level
        int shieldLevel = world.getShip().getShieldLevel();
        shieldLevel *= 10;
        shape.begin(ShapeRenderer.ShapeType.Filled);
        shape.setColor(Color.YELLOW);
        shape.rect(shieldWidth+10, 10, shieldLevel, 25);
        shape.end();
    }

    public void dispose() {
        atlas.dispose();
        font.dispose();
    }
}
