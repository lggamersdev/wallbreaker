package com.silvano.wallbreaker.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Vector2;
import com.silvano.wallbreaker.entity.shots.SimpleShot;
import com.silvano.wallbreaker.entity.Spaceship;
import com.silvano.wallbreaker.entity.SpaceshipUpgrade;
import com.silvano.wallbreaker.entity.SuperShot;
import com.silvano.wallbreaker.screen.GameScreen;
import com.silvano.wallbreaker.utils.GameConstants;
import com.silvano.wallbreaker.world.handlers.ScrollHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by silvano.filho on 02/06/2016.
 */
public class GameWorld {

    public enum GameState {
        READY, RUNNING, PAUSE, GAME_OVER
    }

    private GameScreen gameScreen;
    private GameState state;
    private Spaceship ship;
    private SpaceshipUpgrade upgrade;
    private ScrollHandler scrollHandler;
    private int level;
    private int score;

    public GameWorld(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
        ship = new Spaceship(GameConstants.SPACESHIP_START_POS.cpy(), GameConstants.SPACESHIP_WIDTH, GameConstants.SPACESHIP_HEIGHT);
        upgrade = null;
        scrollHandler = new ScrollHandler();
        state = GameState.READY;
        level = 1;
        score = 0;
    }

    public Spaceship getShip() {
        return ship;
    }

    public SpaceshipUpgrade getUpgrade() {
        return upgrade;
    }

    public ScrollHandler getScrollHandler() {
        return scrollHandler;
    }

    public GameState getState() {
        return state;
    }

    public int getLevel() {
        return level;
    }

    public int getScore() {
        return score;
    }

    /**
     * Base logic of the game
     */
    public void update (float delta) {
        switch (state) {
            case RUNNING:
                updateRunning(delta);
                break;
            default:
                break;
        }
    }

    /**
     * The actual logic of the game
     * @param delta time passed
     */
    public void updateRunning (float delta) {
        // Call each object update
        ship.update(delta);

        if (upgrade != null)
            upgrade.update(delta);

        scrollHandler.update(delta, ship.getCenter().cpy());

        // Collision handling
        List<SimpleShot> shotsToRemove = new ArrayList<SimpleShot>();
        for (SimpleShot shot : ship.getShots()) {
            if (scrollHandler.collides(shot.getBound())) {
                shotsToRemove.add(shot);
                if(scrollHandler.isTargetDestroyed()) {
                    updateScore();
                }
            }
            // Shot that get out of the screens are removed
            if (shot.getPosition().y < GameConstants.START_GAMEPLAY_SCREEN
                    || shot.getPosition().y > GameConstants.SCREEN_HEIGHT
                    || shot.getPosition().x < 0
                    || shot.getPosition().x > GameConstants.SCREEN_WIDTH)
                shotsToRemove.add(shot);
        }

        ship.removeShots(shotsToRemove);

        List<SuperShot> superShotsToRemove = new ArrayList<SuperShot>();
        for (SuperShot superShot : ship.getsuperShots()) {
            scrollHandler.collides(superShot.getBound());
            // Shot that get out of the screens are removed
            if (superShot.getPosition().y < (GameConstants.START_GAMEPLAY_SCREEN + superShot.getHeight()))
                superShotsToRemove.add(superShot);
        }

        ship.removesuperShots(superShotsToRemove);

        if (scrollHandler.collides(ship.getBound())) {
            if (!scrollHandler.hasCollidedWithWall())
                ship.collided(); // remove single point of shield or kill if it has no shield
            else
                ship.collided(scrollHandler.getLevel()); // remove shield or kill according to wall x shield level
        }

        if (upgrade != null && upgrade.collides(ship.getBound())) {
            ship.upgrade(upgrade.getType());
            upgrade = null;
        }

        if (upgrade != null && upgrade.isBelowScreen())
            upgrade = null;

        // Everytime the ship pass through the wall it get a point
        // Also level up logic (from 0 to 20 each 5 level, from 20 each 10
        // level)
        if (scrollHandler.hasPassedWall((int) ship.getPosition().y)) {
            score = score + 10 * level;
            level++;
            scrollHandler.levelUp(level);

            if (level >= 5 && level %2 == 1 && upgrade == null) {
                Random rand = new Random();
                int x = rand.nextInt(430);
                if (x < 20)
                    x = 20;
                upgrade = new SpaceshipUpgrade(new Vector2(x, -5));
            }
        }

        if (!ship.isAlive()) {
            gameOver();
        }
    }

    private void updateScore() {
        score = score + level;
        if(!scrollHandler.hasCollidedWithWall()) {
            score = score + level;
        }
    }

    private int getShipParts() {
        return score / 1000;
    }

    public void gameOver() {
        ship.stop();
        scrollHandler.stop();
        state = GameState.GAME_OVER;
        upgrade = null;
        // set high score level
        Preferences pref = Gdx.app.getPreferences(GameConstants.WALLBREAKER_PREFS);
        if (score > pref.getInteger(GameConstants.HIGHSCORE_PREF, 0)) {
            pref.putInteger(GameConstants.HIGHSCORE_PREF, score);
        }

        // set high level
        if (level > pref.getInteger(GameConstants.HIGHLEVEL_PREF, 0)) {
            pref.putInteger(GameConstants.HIGHLEVEL_PREF, level);
        }

        pref.putInteger(GameConstants.SHIP_PARTS,
                pref.getInteger(GameConstants.SHIP_PARTS, 0)
                + getShipParts());
        pref.flush();
    }

    public int getHighScore() {
        Preferences pref = Gdx.app.getPreferences(GameConstants.WALLBREAKER_PREFS);
        return pref.getInteger(GameConstants.HIGHSCORE_PREF, score);
    }

    public int getHighLevelScore() {
        Preferences pref = Gdx.app.getPreferences(GameConstants.WALLBREAKER_PREFS);
        return pref.getInteger(GameConstants.HIGHLEVEL_PREF, score);
    }

    public void restart() {
        // Restart everything
        ship.restart();
        scrollHandler.restart();
        if (upgrade != null)
            upgrade.restart();
        state = GameState.RUNNING;
    }

    public void ready() {
        level = 1;
        score = 0;
        ship.ready(GameConstants.SPACESHIP_START_POS.cpy());
        scrollHandler.ready();
        state = GameState.READY;
    }

    public void pause() {
        if (state == GameState.RUNNING) {
            ship.stop();
            scrollHandler.stop();
            state = GameState.PAUSE;
            if (upgrade != null)
                upgrade.pause();
        }
    }

    public void backToMenu() {
        gameScreen.backToMenu();
    }

    public void quitGame() {
        Gdx.app.exit();
    }

}
