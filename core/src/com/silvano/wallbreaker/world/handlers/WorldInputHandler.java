package com.silvano.wallbreaker.world.handlers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.silvano.wallbreaker.entity.Spaceship;
import com.silvano.wallbreaker.world.GameWorld;

/**
 * Handle the input
 */
public class WorldInputHandler implements InputProcessor {

    private GameWorld world;
    private Spaceship ship;

    private int oldPosX;
    private int oldPosY;

    public WorldInputHandler(GameWorld world) {
        this.world = world;
        ship = world.getShip();

        oldPosX = 0;
        oldPosY = 0;
    }

    @Override
    public boolean keyDown(int keycode) {
        GameWorld.GameState state = world.getState();
        if (keycode == Input.Keys.BACK && state == GameWorld.GameState.RUNNING) {
            world.pause();
            return true;
        } else if (keycode == Input.Keys.BACK && ( state == GameWorld.GameState.GAME_OVER
                || state == GameWorld.GameState.READY)) {
            world.quitGame();
        } else if (keycode == Input.Keys.BACK && state == GameWorld.GameState.PAUSE) {
            world.backToMenu();
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (world.getState() == GameWorld.GameState.READY) {
            world.restart();
        } else if (world.getState() == GameWorld.GameState.RUNNING) {
            // Assigns the start touch position
            oldPosX = screenX;
            oldPosY = screenY;
        } else if (world.getState() == GameWorld.GameState.GAME_OVER) {
            world.ready();
        } else if (world.getState() == GameWorld.GameState.PAUSE)
            world.restart();
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (world.getState() == GameWorld.GameState.RUNNING) {
            // stop movement and set idle flag to true
            ship.setIsIdle(true);
            ship.setSpeed(Vector2.Zero);
        }
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (world.getState() == GameWorld.GameState.RUNNING) {
            // Get the distance of swipe movement and define it as the distance the ship will move (speed)
            int moveX = (screenX - oldPosX)/2;
            int moveY = (screenY - oldPosY)/2;

            // set max speed
            if (moveX > 20)
                moveX = 20;
            else if (moveX < -20)
                moveX = -20;
            if (moveY > 20)
                moveY = 20;
            else if (moveY < -20)
                moveY = -20;

            // if the distance of the swipe movement is bigger than 0 than set the speed, otherwise
            // let the ship stop
            if (Math.abs(moveX) > 0 || Math.abs(moveY) > 0) {
                ship.setSpeed(new Vector2(moveX, moveY));
            } else
                ship.setSpeed(Vector2.Zero);
            // the ship is not idle since the touch continues
            ship.setIsIdle(false);
            // Assigns the current position as old
            oldPosX = screenX;
            oldPosY = screenY;
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
