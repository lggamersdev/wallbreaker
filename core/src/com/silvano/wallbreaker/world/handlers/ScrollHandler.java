package com.silvano.wallbreaker.world.handlers;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.silvano.wallbreaker.entity.Turret;
import com.silvano.wallbreaker.utils.GameConstants;
import com.silvano.wallbreaker.utils.WallUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import sun.rmi.runtime.Log;

/**
 * Created by silvano.filho on 06/06/2016.
 */
public class ScrollHandler {

    private static final int SCROLL_SPEED = 100;
    private static final int GAP = -400;

    private WallUtils wall1, wall2;
    private Map<Integer, Integer> turretsPosMap;
    private List<Turret> turrets1;
    private List<Turret> turrets2;
    private boolean isPointCounted;
    private boolean hasCollidedWithWall;
    private boolean isTargetDestroyed;

    public ScrollHandler() {
        wall1 = new WallUtils(GameConstants.START_GAMEPLAY_SCREEN, SCROLL_SPEED);
        wall2 = new WallUtils(wall1.getTail() + GAP, SCROLL_SPEED);

        // turrets creation
        turrets1 = new ArrayList<Turret>();
        turrets2 = new ArrayList<Turret>();

        // randomly define the position
        turretsPosMap = new HashMap<Integer, Integer>();
        setTurretPosMap();
        addTurrets(true);

        isPointCounted = false;
        hasCollidedWithWall = false;
        isTargetDestroyed = false;
    }

    public WallUtils getWall1() {
        return wall1;
    }

    public WallUtils getWall2() {
        return wall2;
    }

    public List<Turret> getTurrets() {
        List<Turret> allTurrets = new ArrayList<Turret>();
        allTurrets.addAll(turrets1);
        allTurrets.addAll(turrets2);
        return allTurrets;
    }

    /**
     * Add turrets "allowed" positions
     */
    private void setTurretPosMap() {
        turretsPosMap.clear();
        turretsPosMap.put(0, 5);
        turretsPosMap.put(1, GameConstants.SCREEN_WIDTH - GameConstants.TURRET_WIDTH - 5);
        turretsPosMap.put(2, GameConstants.TILE_WIDTH_HEIGHT * 4 + 5);
        turretsPosMap.put(3, GameConstants.TILE_WIDTH_HEIGHT * 10 + 5);
    }

    /**
     * Add turrets at random in the "allowed" positions
     * @param isCollidable only collidable turrets should be added and rendered
     */
    private void addTurrets(boolean isCollidable) {
        if (turretsPosMap.isEmpty())
            return;

        Random rand = new Random();
        Object keys[] = turretsPosMap.keySet().toArray();
        int randomKey = rand.nextInt(keys.length);

        Turret turret1 = new Turret(turretsPosMap.get(keys[randomKey]), wall1.getTail()
                + GameConstants.TILE_WIDTH_HEIGHT, SCROLL_SPEED);
        turret1.setCollidable(isCollidable);
        turrets1.add(turret1);
        Turret turret2 = new Turret(turretsPosMap.get(keys[randomKey]), wall2.getTail()
                + GameConstants.TILE_WIDTH_HEIGHT, SCROLL_SPEED);
        turret2.setCollidable(isCollidable);
        turrets2.add(turret2);
    }

    /**
     * Verify if the spaceship has passed throught the closest wall
     * @param posY the Y position of the spaceship
     * @return true if the spaceship has passed, false if not
     */
    public boolean hasPassedWall(int posY) {
        Integer diff1 = posY - wall1.getTail();
        Integer diff2 = posY - wall2.getTail();

        if ((diff1 < 0 || diff2 < 0) && !isPointCounted) {
            isPointCounted = true;
            return true;
        }

        return false;
    }

    public boolean hasCollidedWithWall() {
        return hasCollidedWithWall;
    }

    public boolean isTargetDestroyed() {
        return isTargetDestroyed;
    }

    public int getLevel() {
        if (wall1.isOnScreen())
            return wall1.getLevel();

        return wall2.getLevel();
    }

    /**
     * Level up logic for tiles (wall) and turrets
     * @param level current spaceship level (how many level it has made)
     */
    public void levelUp(int level) {
        if (level == 2 || level == 10 || level == 20) {
            addTurrets(false);
        } else if (level == 5 || level == 15 || level == 30) {
            wall1.levelUp();
            wall2.levelUp();
        }

        //for (Turret t : getTurrets()) {
        //    t.levelUp();
        //}
    }

    public void stop() {
        wall1.stop();
        wall2.stop();
        for (Turret t : getTurrets()) {
            t.stop();
        }
    }

    public void ready() {
        wall1.reset(0);
        wall1.resetLevel();
        wall2.reset(wall1.getTail() + GAP);
        wall2.resetLevel();

        turrets1.clear();
        turrets2.clear();
        setTurretPosMap();
        addTurrets(true);
    }

    public void restart() {
        wall1.restart();
        wall2.restart();
        for (Turret t : getTurrets()) {
            t.restart();
        }
    }

    public void update(float delta, Vector2 shipPos) {
        wall1.update(delta, wall2.getTail() + GAP);
        wall2.update(delta, wall1.getTail() + GAP);

        if (wall1.hasResetted() || wall2.hasResetted())
            isPointCounted = false;

        updateTurret(turrets1, delta, shipPos, wall1.getTail() + GameConstants.TILE_WIDTH_HEIGHT);
        updateTurret(turrets2, delta, shipPos, wall2.getTail() + GameConstants.TILE_WIDTH_HEIGHT);
    }

    public void updateTurret(List<Turret> turrets, float delta, Vector2 shipPos, float newPosY) {
        setTurretPosMap();
        int numTurrets = turrets.size();
        Random rand = new Random();

        for (Turret t : turrets) {
            t.setShipPosition(shipPos);
            t.update(delta);
            if (t.isScrolledDown()) {
                if (numTurrets <= 4) {
                    Object keys[] = turretsPosMap.keySet().toArray();
                    int randomKey = rand.nextInt(keys.length);

                    t.reset(newPosY, turretsPosMap.get(keys[randomKey]));
                    turretsPosMap.remove(keys[randomKey]);
                } else
                    t.reset(newPosY);
            }
        }
    }

    public boolean collides(Rectangle rect) {
        boolean collided = false;
        hasCollidedWithWall = false;
        isTargetDestroyed = false;

        for (Turret t : getTurrets()) {
            if (t.onGameplayScreen() && t.collides(rect)) {
                isTargetDestroyed = t.isDead();
                return true;
            }
        }

        if (wall1.collides(rect)) {
            collided = true;
            hasCollidedWithWall = true;
            isTargetDestroyed = wall1.isWallDestroyed();

        } else if (wall2.collides(rect)) {
            collided = true;
            hasCollidedWithWall = true;
            isTargetDestroyed = wall2.isWallDestroyed();
        }

        return collided;
    }

    public boolean collides(Circle circle) {
        boolean collided = false;
        hasCollidedWithWall = false;
        isTargetDestroyed = false;

        for (Turret t : getTurrets()) {
            if (t.onGameplayScreen() && t.collides(circle)) {
                isTargetDestroyed = t.isDead();
                return true;
            }
        }

        if (wall1.collides(circle)) {
            collided = true;
            hasCollidedWithWall = true;
            isTargetDestroyed = wall1.isWallDestroyed();
        } else if (wall2.collides(circle)) {
            collided = true;
            hasCollidedWithWall = true;
            isTargetDestroyed = wall2.isWallDestroyed();
        }

        return collided;
    }

}
