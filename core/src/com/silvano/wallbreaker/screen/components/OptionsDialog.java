package com.silvano.wallbreaker.screen.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.silvano.wallbreaker.WallbreakerGame;
import com.silvano.wallbreaker.utils.GameConstants;

/**
 * Created by rodrigo.camara on 06/01/2017.
 */

public class OptionsDialog {
    private Batch mBatch;
    private TextureRegion dialogBG;
    private WallbreakerGame game;
    private float X, Y;

    public OptionsDialog(WallbreakerGame game, ImageButton imgBtn) {
        this.mBatch = game.getBatch();
        this.game = game;
        //Don't ask questions, trust these numbers.
        X = imgBtn.getX() - 55;
        Y = imgBtn.getY() - 15;
    }

    public void createDialog() {
        mBatch.begin();
        mBatch.enableBlending();

        dialogBG = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_BG_MENU_BUTTONS, Texture.class));
        if (!dialogBG.isFlipY())
            dialogBG.flip(false, true);

        mBatch.draw(dialogBG, X, Y, 350, 250);
        mBatch.end();
    }

    private void drawText() {

    }

}
