package com.silvano.wallbreaker.screen.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.silvano.wallbreaker.WallbreakerGame;
import com.silvano.wallbreaker.utils.GameConstants;
import com.silvano.wallbreaker.utils.TextUtils;

/**
 * Created by rodrigo.camara on 06/01/2017.
 */

public class CreditsDialog {
    private Batch mBatch;
    private TextureRegion dialogBG;
    private WallbreakerGame game;
    private float X, Y;
    private BitmapFont optionsFont;

    public CreditsDialog(WallbreakerGame game, ImageButton imgBtn) {
        this.mBatch = game.getBatch();
        this.game = game;
        //Don't ask questions, trust these numbers.
        X = imgBtn.getX() - 55;
        Y = imgBtn.getY() - 15;
        optionsFont = TextUtils.createFont();
    }

    public void createDialog() {
        mBatch.begin();
        mBatch.enableBlending();

        dialogBG = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_BG_MENU_BUTTONS, Texture.class));
        if (!dialogBG.isFlipY())
            dialogBG.flip(false, true);

        mBatch.draw(dialogBG, X, Y, 350, 250);
        mBatch.end();
        drawText();
    }

    private void drawText() {
        mBatch.begin();
        mBatch.enableBlending();
        String creditsText = "Credits";
        Vector2 centerOfText = TextUtils.getCenterOfText(optionsFont, creditsText);
        optionsFont.setColor(Color.BLACK);
        optionsFont.draw(mBatch, creditsText, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfText.x),(GameConstants.SCREEN_HEIGHT / 2) - 25);

        String creditsName = "O Márcio é MUITO RUIM no Overwatch";
        centerOfText = TextUtils.getCenterOfText(optionsFont, creditsName);
        //Vou Mudar...Não chora
        optionsFont.draw(mBatch, creditsName, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfText.x),(GameConstants.SCREEN_HEIGHT / 2) - 5);
        optionsFont.draw(mBatch, creditsName, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfText.x),(GameConstants.SCREEN_HEIGHT / 2) + 15);
        optionsFont.draw(mBatch, creditsName, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfText.x),(GameConstants.SCREEN_HEIGHT / 2) + 35);
        optionsFont.draw(mBatch, creditsName, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfText.x),(GameConstants.SCREEN_HEIGHT / 2) + 55);
        optionsFont.draw(mBatch, creditsName, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfText.x),(GameConstants.SCREEN_HEIGHT / 2) + 75);
        mBatch.end();
    }

}
