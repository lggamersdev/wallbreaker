package com.silvano.wallbreaker.screen.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.silvano.wallbreaker.WallbreakerGame;
import com.silvano.wallbreaker.screen.GameScreen;
import com.silvano.wallbreaker.screen.MenuScreen;
import com.silvano.wallbreaker.utils.GameConstants;

/**
 * Created by rodrigo.camara on 04/01/2017.
 */

public class MenuScreenButtons {
    ImageButton startGameButton, optionsButton, creditsButton, facebookButton, twitterButton, googlePlayButton;
    private TextureRegion startButtonTexture, optionsButtonTexture, creditsButtonTexture, facebookButtonTexture, twitterButtonTexture, googlePlayButtonTexture;
    private WallbreakerGame game;
    private static final int DEFAULT_MARGIN = 10;
    private CreditsDialog creditsDialog;
    public MenuScreenButtons(WallbreakerGame game) {
        this.game = game;
    }

    public ImageButton createStartGameButton() {
        TextureRegionDrawable myTexRegionDrawable = new TextureRegionDrawable(getStartButtonTexture());
        startGameButton = new ImageButton(myTexRegionDrawable); //Set the button up
        startGameButton.setX((GameConstants.SCREEN_WIDTH / 2) - (getStartButtonTexture().getRegionWidth() / 2));
        startGameButton.setY((GameConstants.SCREEN_HEIGHT / 2) - (getStartButtonTexture().getRegionHeight() / 2));
        startGameButton.addListener(startGameEventListener());
        return startGameButton;
    }

    //Public to get BG size to Menu
    public ImageButton getStartGameButton() {
        return startGameButton;
    }

    public EventListener startGameEventListener() {
        return new EventListener() {
            @Override
            public boolean handle(Event event) {
                // Take the player to Game Screen
                game.getScreen().dispose();
                game.setScreen(new GameScreen(game));
                return false;
            }
        };
    }

    public TextureRegion getStartButtonTexture() {
        startButtonTexture = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_START_MENU_BUTTONS, Texture.class));
        if (!startButtonTexture.isFlipY())
            startButtonTexture.flip(false, true);
        return startButtonTexture;
    }

    public ImageButton createOptionsButton() {
        TextureRegionDrawable myTexRegionDrawable = new TextureRegionDrawable(getOptionsButtonTexture());
        optionsButton = new ImageButton(myTexRegionDrawable); //Set the button up
        optionsButton.setX((GameConstants.SCREEN_WIDTH / 2) - (getStartButtonTexture().getRegionWidth() / 2));
        optionsButton.setY(startGameButton.getY() + (startGameButton.getHeight() + DEFAULT_MARGIN));
        optionsButton.addListener(optionsEventListener());
        return optionsButton;
    }

    public EventListener optionsEventListener() {
        return new EventListener() {
            @Override
            public boolean handle(Event event) {
                MenuScreen.showOptions =true;
                return false;
            }
        };
    }

    private TextureRegion getOptionsButtonTexture() {
        optionsButtonTexture = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_OPTIONS_MENU_BUTTONS, Texture.class));
        if (!optionsButtonTexture.isFlipY())
            optionsButtonTexture.flip(false, true);
        return optionsButtonTexture;
    }

    public ImageButton createCreditsButton() {
        TextureRegionDrawable myTexRegionDrawable = new TextureRegionDrawable(getCreditsButtonTexture());
        creditsButton = new ImageButton(myTexRegionDrawable); //Set the button up
        creditsButton.setX((GameConstants.SCREEN_WIDTH / 2) - (getOptionsButtonTexture().getRegionWidth() / 2));
        creditsButton.setY(optionsButton.getY() + (optionsButton.getHeight() + DEFAULT_MARGIN));
        creditsButton.addListener(creditsEventListener());
        return creditsButton;
    }

    public EventListener creditsEventListener() {
        return new EventListener() {
            @Override
            public boolean handle(Event event) {
                MenuScreen.showCredits =true;
                return false;
            }
        };
    }

    private TextureRegion getCreditsButtonTexture() {
        creditsButtonTexture = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_CREDITS_MENU_BUTTONS, Texture.class));
        if (!creditsButtonTexture.isFlipY())
            creditsButtonTexture.flip(false, true);
        return creditsButtonTexture;
    }

    public ImageButton createTwitterButton() {
        TextureRegionDrawable myTexRegionDrawable = new TextureRegionDrawable(getTwitterButtonTexture());
        twitterButton = new ImageButton(myTexRegionDrawable); //Set the button up
        twitterButton.setX((GameConstants.SCREEN_WIDTH - getTwitterButtonTexture().getTexture().getWidth()) -5);
        twitterButton.setY(DEFAULT_MARGIN);
        twitterButton.addListener(twitterButtonEventListener());
        return twitterButton;
    }

    public EventListener twitterButtonEventListener() {
        return new EventListener() {
            @Override
            public boolean handle(Event event) {
                return false;
            }
        };
    }

    private TextureRegion getTwitterButtonTexture() {
        twitterButtonTexture = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_MENU_TWITTER_BUTTON, Texture.class));
        if (!twitterButtonTexture.isFlipY())
            twitterButtonTexture.flip(false, true);
        return twitterButtonTexture;
    }

    public ImageButton createFacebookButton() {
        TextureRegionDrawable myTexRegionDrawable = new TextureRegionDrawable(getFacebookButtonTexture());
        facebookButton = new ImageButton(myTexRegionDrawable); //Set the button up
        facebookButton.setX((GameConstants.SCREEN_WIDTH - (getTwitterButtonTexture().getTexture().getWidth()*2)) - 5);
        facebookButton.setY(DEFAULT_MARGIN);
        facebookButton.addListener(facebookButtonEventListener());
        return facebookButton;
    }

    public EventListener facebookButtonEventListener() {
        return new EventListener() {
            @Override
            public boolean handle(Event event) {
                return false;
            }
        };
    }

    private TextureRegion getFacebookButtonTexture() {
        facebookButtonTexture = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_MENU_FACEBOOK_BUTTON, Texture.class));
        if (!facebookButtonTexture.isFlipY())
            facebookButtonTexture.flip(false, true);
        return facebookButtonTexture;
    }

    public ImageButton createGooglePlayButton() {
        TextureRegionDrawable myTexRegionDrawable = new TextureRegionDrawable(getGooglePlayButtonTexture());
        googlePlayButton = new ImageButton(myTexRegionDrawable); //Set the button up
        googlePlayButton.setX((GameConstants.SCREEN_WIDTH - (getGooglePlayButtonTexture().getTexture().getWidth()*3)) - 5);
        googlePlayButton.setY(DEFAULT_MARGIN);
        googlePlayButton.addListener(googlePlayButtonEventListener());
        return googlePlayButton;
    }

    public EventListener googlePlayButtonEventListener() {
        return new EventListener() {
            @Override
            public boolean handle(Event event) {
                return false;
            }
        };
    }

    private TextureRegion getGooglePlayButtonTexture() {
        googlePlayButtonTexture = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_MENU_GOOGLE_PLAY_BUTTON, Texture.class));
        if (!googlePlayButtonTexture.isFlipY())
            googlePlayButtonTexture.flip(false, true);
        return googlePlayButtonTexture;
    }
    //Public to get BG size to Menu
    public ImageButton getGooglePlayButton() {
        return googlePlayButton;
    }
}