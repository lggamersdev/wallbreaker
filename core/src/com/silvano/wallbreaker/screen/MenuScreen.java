package com.silvano.wallbreaker.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.silvano.wallbreaker.WallbreakerGame;
import com.silvano.wallbreaker.screen.components.CreditsDialog;
import com.silvano.wallbreaker.screen.components.MenuScreenButtons;
import com.silvano.wallbreaker.screen.components.OptionsDialog;
import com.silvano.wallbreaker.utils.GameConstants;
import com.silvano.wallbreaker.utils.LanguageUtils;
import com.silvano.wallbreaker.utils.TextUtils;

/**
 * Created by silvano.filho on 07/06/2016.
 */
public class MenuScreen extends ScreenAdapter {
    private WallbreakerGame game;
    private BitmapFont optionsFont;

    private OrthographicCamera cam;
    private MenuScreenButtons menuScreenButtons;
    //Menu
    private Stage menuStage;
    private TextureRegion menuBG;
    private Preferences mSharedPrefs;
    float X, Y;

    //Dialogs
    public static boolean showCredits, showOptions = false;
    private CreditsDialog creditsDialog;
    private OptionsDialog optionsDialog;

    public MenuScreen(WallbreakerGame game) {
        this.game = game;
        optionsFont = TextUtils.createFont();

        cam = new OrthographicCamera();
        cam.setToOrtho(true, GameConstants.SCREEN_WIDTH, GameConstants.SCREEN_HEIGHT);

        menuScreenButtons = new MenuScreenButtons(game);
        menuStage = new Stage(new ScreenViewport()); // Setup UI
        // Rotate Camera, since the game camera is rotated we need our buttons to be as well.
        menuStage.getViewport().setCamera(cam);
        mSharedPrefs = Gdx.app.getPreferences(GameConstants.WALLBREAKER_PREFS);
    }

    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(menuStage);
        initializeStageInputs();
        initializeMenuComponents();
    }

    private void initializeMenuComponents() {
        menuStage.addActor(menuScreenButtons.createStartGameButton());
        menuStage.addActor(menuScreenButtons.createOptionsButton());
        menuStage.addActor(menuScreenButtons.createCreditsButton());
        menuStage.addActor(menuScreenButtons.createTwitterButton());
        menuStage.addActor(menuScreenButtons.createFacebookButton());
        menuStage.addActor(menuScreenButtons.createGooglePlayButton());

        menuBG = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_BG_MENU_BUTTONS, Texture.class));
        if (!menuBG.isFlipY())
            menuBG.flip(false, true);
        X = menuScreenButtons.getStartGameButton().getX() - 15;
        Y = menuScreenButtons.getStartGameButton().getY() - 15;
    }

    private void initializeStageInputs() {
        menuStage.addListener(keyDownListener());
        menuStage.addListener(touchDownListener());

    }

    private InputListener touchDownListener() {
        return new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (showCredits) {
                    showCredits = false;
                } else if (showOptions) {
                    showOptions = false;
                }
                return false;
            }
        };
    }

    private InputListener keyDownListener() {
        return new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keyCode) {
                if (keyCode == Input.Keys.BACK) {
                    if (showCredits) {
                        showCredits = false;
                    } else if (showOptions) {
                        showOptions = false;
                    } else {
                        //TODO implement dialog
                        Gdx.app.exit();
                    }
                }
                return true;
            }
        };
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        // We draw a black background. This prevents flickering.
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        SpriteBatch batch = game.getBatch();
        batch.setProjectionMatrix(cam.combined);

        //Background
        drawBackground(batch);

        //Menu, adding buttons to Stage.
        drawMenu(batch);

        //HighScore and Ship
        drawHighScore(batch);

        //Credits on Screen
        drawCredits(batch);

        //Dialogs when requested
        drawDialogs();
    }

    private void drawBackground(Batch batch) {
        batch.begin();
        batch.disableBlending();
        TextureRegion background = new TextureRegion(game.getManager().get(GameConstants.TEXTURE_MAIN_MENU, Texture.class));
        if (!background.isFlipY())
            background.flip(false, true);
        batch.draw(background, 0, 0, GameConstants.SCREEN_WIDTH, GameConstants.SCREEN_HEIGHT);
        batch.end();
    }

    private void drawMenu(Batch batch) {
        batch.begin();
        batch.enableBlending();

        //TODO Improve BG size calc and image quality
        batch.draw(menuBG, X, Y, menuScreenButtons.getStartGameButton().getWidth() + 30, 190);
        batch.end();
        menuStage.act(Gdx.graphics.getDeltaTime()); //Perform UI logic
        menuStage.draw(); // Actually Draw Menu
    }

    private void drawHighScore(Batch batch) {
        batch.begin();
        batch.enableBlending();
        String highscoreText = LanguageUtils.getLanguageBundle().get("highScore") + mSharedPrefs.getInteger(GameConstants.HIGHSCORE_PREF, 0);
        Vector2 centerOfHighscoreText = TextUtils.getCenterOfText(optionsFont, highscoreText);
        optionsFont.draw(batch, highscoreText, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfHighscoreText.x),
                (menuScreenButtons.getStartGameButton().getWidth() + 30) - (centerOfHighscoreText.y * 2));
        batch.end();
    }

    private void drawCredits(Batch batch) {
        batch.begin();
        batch.enableBlending();
        String creditsText = "by Macaxera Games";
        Vector2 centerOfHighscoreText = TextUtils.getCenterOfText(optionsFont, creditsText);
        optionsFont.draw(batch, creditsText, TextUtils.getWidthOrHeightFontPosition(GameConstants.SCREEN_WIDTH, centerOfHighscoreText.x), GameConstants.SCREEN_HEIGHT - 20);
        batch.end();
    }

    private void drawDialogs() {
        if (showCredits) {
            creditsDialog = new CreditsDialog(game, menuScreenButtons.getStartGameButton());
            creditsDialog.createDialog();
        } else if (showOptions) {
            optionsDialog = new OptionsDialog(game, menuScreenButtons.getStartGameButton());
            optionsDialog.createDialog();
        }
    }
}