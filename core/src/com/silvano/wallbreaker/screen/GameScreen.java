package com.silvano.wallbreaker.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.silvano.wallbreaker.WallbreakerGame;
import com.silvano.wallbreaker.world.handlers.WorldInputHandler;
import com.silvano.wallbreaker.world.GameWorld;
import com.silvano.wallbreaker.world.WorldRenderer;

/**
 * Created by silvano.filho on 02/06/2016.
 */
public class GameScreen extends ScreenAdapter {

    private WallbreakerGame game;
    private GameWorld world;
    private WorldRenderer renderer;

    private float runTime;

    public GameScreen (WallbreakerGame game) {
        this.game = game;
        world = new GameWorld(this);
        renderer = new WorldRenderer(world, game.getManager(), game.getBatch(), game.getShape());
        runTime = 0;
    }

    public void backToMenu() {
        this.dispose();
        game.setScreen(new MenuScreen(game));
    }

    @Override
    public void show() {
        super.show();

        Gdx.input.setInputProcessor(new WorldInputHandler(world));
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        runTime += delta;

        world.update(delta);
        renderer.render(runTime);
    }

    @Override
    public void pause() {
        super.pause();
        if (world.getState() == GameWorld.GameState.RUNNING)
            world.pause();
    }

    @Override
    public void dispose() {
        super.dispose();
        renderer.dispose();
    }
}
