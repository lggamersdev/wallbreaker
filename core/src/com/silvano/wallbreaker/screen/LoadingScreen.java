package com.silvano.wallbreaker.screen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.silvano.wallbreaker.WallbreakerGame;
import com.silvano.wallbreaker.screen.components.LoadingBar;
import com.silvano.wallbreaker.utils.GameConstants;

/**
 * Loading screen, this is where all the assets are loaded into memory before starting the game
 */
public class LoadingScreen extends ScreenAdapter {

    private final float MINIMUM_TIME = 4; // the duration of this screen is until finish loading assets or 4s

    private WallbreakerGame game;
    private LoadingBar loadingBar;
    private Stage stage;

    private Image logo;
    private Image loadingFrame;
    private Image loadingBarHidden;
    private Image screenBg;
    private Image loadingBg;
    private Animation anim;

    private float percent;
    private float startX, endX;
    private float timePassed = 0;

    public LoadingScreen(WallbreakerGame game) {
        this.game = game;
    }

    @Override
    public void show() {
        super.show();
        // Assets for this screen
        game.getManager().load(GameConstants.TEXTURE_ATLAS_LOADING, TextureAtlas.class);
        game.getManager().finishLoading();

        // Initialize the stage where we will place everything
        stage = new Stage();

        // Grab the regions from the atlas and create some images
        TextureAtlas atlas = game.getManager().get(GameConstants.TEXTURE_ATLAS_LOADING, TextureAtlas.class);
        logo = new Image(atlas.findRegion("libgdx-logo"));
        loadingFrame = new Image(atlas.findRegion("loading-frame"));
        loadingBarHidden = new Image(atlas.findRegion("loading-bar-hidden"));
        screenBg = new Image(atlas.findRegion("screen-bg"));
        loadingBg = new Image(atlas.findRegion("loading-frame-bg"));

        // Add the loading bar animation
        anim = new Animation(0.05f, atlas.findRegions("loading-bar-anim") );
        anim.setPlayMode(Animation.PlayMode.LOOP_REVERSED);
        loadingBar = new LoadingBar(anim);

        stage.addActor(screenBg);
        stage.addActor(loadingBar);
        stage.addActor(loadingBg);
        stage.addActor(loadingBarHidden);
        stage.addActor(loadingFrame);
        stage.addActor(logo);

        // Other assets to be loaded
        game.getManager().load(GameConstants.TEXTURE_ATLAS, TextureAtlas.class);
        game.getManager().load(GameConstants.TEXTURE_MAIN_MENU, Texture.class);
        game.getManager().load(GameConstants.TEXTURE_START_MENU_BUTTONS, Texture.class);
        game.getManager().load(GameConstants.TEXTURE_OPTIONS_MENU_BUTTONS, Texture.class);
        game.getManager().load(GameConstants.TEXTURE_CREDITS_MENU_BUTTONS, Texture.class);
        game.getManager().load(GameConstants.TEXTURE_BG_MENU_BUTTONS, Texture.class);
        game.getManager().load(GameConstants.TEXTURE_MENU_FACEBOOK_BUTTON, Texture.class);
        game.getManager().load(GameConstants.TEXTURE_MENU_TWITTER_BUTTON, Texture.class);
        game.getManager().load(GameConstants.TEXTURE_MENU_GOOGLE_PLAY_BUTTON,Texture.class);
    }

    @Override
    public void resize(int width, int height) {
        // Make the background fill the screen
        screenBg.setSize(width, height);

        // Place the logo in the middle of the screen and 100 px up
        logo.setX((width - logo.getWidth()) / 2);
        logo.setY((height - logo.getHeight()) / 2 + 100);

        // Place the loading frame in the middle of the screen
        loadingFrame.setX((stage.getWidth() - loadingFrame.getWidth()) / 2);
        loadingFrame.setY((stage.getHeight() - loadingFrame.getHeight()) / 2);

        // Place the loading bar at the same spot as the frame, adjusted a few px
        loadingBar.setX(loadingFrame.getX() + 15);
        loadingBar.setY(loadingFrame.getY() + 5);

        // Place the image that will hide the bar on top of the bar, adjusted a few px
        loadingBarHidden.setX(loadingBar.getX() + 35);
        loadingBarHidden.setY(loadingBar.getY() - 3);
        // The start position and how far to move the hidden loading bar
        startX = loadingBarHidden.getX();
        endX = 440;

        // The rest of the hidden bar
        loadingBg.setSize(450, 50);
        loadingBg.setX(loadingBarHidden.getX() + 30);
        loadingBg.setY(loadingBarHidden.getY() + 3);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        timePassed += delta;

        renderProgressBar();

        if (game.getManager().update() && timePassed >= MINIMUM_TIME) {
            this.dispose();
            game.setScreen(new MenuScreen(game));
        }
    }

    private void renderProgressBar() {
        // Interpolate the percentage to make it more smooth
        percent = Interpolation.linear.apply(percent, game.getManager().getProgress(), 0.1f);

        // Update positions (and size) to match the percentage
        loadingBarHidden.setX(startX + endX * percent);
        loadingBg.setX(loadingBarHidden.getX() + 30);
        loadingBg.setWidth(450 - 450 * percent);
        loadingBg.invalidate();

        // Show the loading screen
        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
    }
}
