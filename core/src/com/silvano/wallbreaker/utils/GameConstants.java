package com.silvano.wallbreaker.utils;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by silvano.filho on 02/06/2016.
 */
public class GameConstants {

    // DEBUG
    public static final boolean IS_DEBUG = true;

    // SCREEN SIZE
    public static final int SCREEN_WIDTH = 450;
    public static final int SCREEN_HEIGHT = 600;
    public static final int START_GAMEPLAY_SCREEN = 40;

    // PREFERENCES
    public static final String WALLBREAKER_PREFS = "WALLBREAKER_PREFERENCES";
    public static final String HIGHSCORE_PREF = "RECORD_PREFERENCE";
    public static final String HIGHLEVEL_PREF = "RECORD_LEVEL_PREFERENCE";
    public static final String SHIP_PARTS = "SHIP_PARTS_PREFERENCE";

    //SPACESHIP PARAMETERS
    public static final Vector2 SPACESHIP_START_POS = new Vector2(210, 485);
    public static final int SPACESHIP_WIDTH = 30;
    public static final int SPACESHIP_HEIGHT = 43;

    //WALL / TILES PARAMETERS
    public static final int TILE_WIDTH_HEIGHT = 30;

    // TURRETS PARAMETERS
    public static final int TURRET_WIDTH = 16;
    public static final int TURRET_HEIGHT = 30;

    // TEXTURES
    public static final String TEXTURE_ATLAS = "gfx/textures.pack";
    public static final String TEXTURE_ATLAS_LOADING = "gfx/loading/loading.pack";
    public static final String TEXTURE_MAIN_MENU = "gfx/mainmenu.png";
    public static final String TEXTURE_START_MENU_BUTTONS = "gfx/StartButton.png";
    public static final String TEXTURE_OPTIONS_MENU_BUTTONS = "gfx/OptionsButton.png";
    public static final String TEXTURE_CREDITS_MENU_BUTTONS = "gfx/CreditsButton.png";
    public static final String TEXTURE_BG_MENU_BUTTONS = "gfx/MenuBG.png";
    public static final String TEXTURE_MENU_FACEBOOK_BUTTON = "gfx/FacebookSmall.png";
    public static final String TEXTURE_MENU_TWITTER_BUTTON = "gfx/TwitterSmall.png";
    public static final String TEXTURE_MENU_GOOGLE_PLAY_BUTTON = "gfx/GooglePlaySmall.png";
}
