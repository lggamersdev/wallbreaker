package com.silvano.wallbreaker.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by silvano.filho on 15/06/2016.
 */
public class TextUtils {

    public static float getWidthOrHeightFontPosition(float screenSize, float textSize) {
        if (screenSize <=0 || textSize <=0 )
            return 0;

        return (screenSize/2)-(textSize/2);
    }

    public static Vector2 getCenterOfText(BitmapFont font, String text) {
        if (font == null || text.isEmpty())
            return Vector2.Zero;

        GlyphLayout glyph = new GlyphLayout();
        glyph.setText(font, text);
        return new Vector2(glyph.width, glyph.height);
    }

    public static float getEndOfText(BitmapFont font, String text) {
        if (font == null || text.isEmpty())
            return 0;

        GlyphLayout glyph = new GlyphLayout();
        glyph.setText(font, text);
        return new Vector2(glyph.width, glyph.height).x;
    }

    public static float getBelowText(BitmapFont font, String text) {
        if (font == null || text.isEmpty())
            return 0;

        GlyphLayout glyph = new GlyphLayout();
        glyph.setText(font, text);
        return new Vector2(glyph.width, glyph.height).y;
    }

    /**
     * Create a font using latin accepted characters in the default ttf file (Google's Roboto)
     * @return a BitmapFont containing latin accepted characters
     */
    public static BitmapFont createFont() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("data/font/Roboto-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 18;
        parameter.flip = true;
        BitmapFont trueTypeFont = generator.generateFont(parameter);
        generator.dispose();

        return trueTypeFont;
    }

    /**
     * Create a font using the defined characters in the default ttf file
     * @param characters the set of characters which will be included in the font
     * @return a BitmapFont containing the defined characters
     */
    public static BitmapFont createFont(String characters) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("data/bsicaunicode.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 20;
        parameter.flip = true;
        parameter.characters = characters;
        BitmapFont trueTypeFont = generator.generateFont(parameter);
        generator.dispose();

        return trueTypeFont;
    }

    /**
     * Create a font using the defined characters in the defined ttf file
     * @param pathTTF the path of the defined ttf file
     * @param characters the set of characters which will be included in the font
     * @return a BitmapFont containing the defined characters
     */
    public static BitmapFont createFont(String pathTTF, String characters) {
        BitmapFont trueTypeFont = null;
        try {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(pathTTF));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 20;
            parameter.flip = true;
            parameter.characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\\\/?-+=()*&.;:,{}\\\"´`'<>";
            trueTypeFont = generator.generateFont(parameter);
            generator.dispose();
        } catch (Exception e) {
            // TODO print a better log error
            e.printStackTrace();
        }

        return trueTypeFont;
    }

}
