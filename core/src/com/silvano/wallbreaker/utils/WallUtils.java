package com.silvano.wallbreaker.utils;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.silvano.wallbreaker.entity.Tile;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages all the tiles in each wall in the same way to assures the same behavior
 */
public class WallUtils {

    public static final int NUM_TILES = 15;

    private List<Tile> tiles;
    private boolean hasResetted;
    private boolean wallDestroyed;

    public WallUtils(int posY, int scrollSpeed) {
        tiles = new ArrayList<Tile>();
        for (int i = 0; i < NUM_TILES; i++) {
            Tile t1 = new Tile((i * GameConstants.TILE_WIDTH_HEIGHT), posY, scrollSpeed);
            tiles.add(t1);
        }

        hasResetted = false;
        wallDestroyed = false;
    }

    public int getLevel() {
        return tiles.get(0).getLevel();
    }

    public int getTail() {
        if (tiles.isEmpty())
            return -1;
        return tiles.get(0).getTail();
    }

    public boolean isOnScreen() {
        return tiles.get(0).onGameplayScreen();
    }

    public boolean hasResetted() {
        return hasResetted;
    }

    public boolean isWallDestroyed() {
        return wallDestroyed;
    }

    public void levelUp() {
        for (Tile t : tiles)
            t.levelUp();
    }

    public void resetLevel() {
        for (Tile t : tiles)
            t.resetLevel();
    }

    public void stop() {
        for (Tile t : tiles)
            t.stop();
    }

    public void reset(float newPosY) {
        for (Tile t : tiles) {
            t.reset(newPosY);
        }
    }

    public void restart() {
        for (Tile t : tiles) {
            t.restart();
        }
    }

    public void update(float delta, float newPosY) {
        for (Tile t : tiles) {
            t.update(delta);
            if (t.isScrolledDown()) {
                t.reset(newPosY);
                hasResetted = true;
            } else
                hasResetted = false;
        }
    }

    public void render(SpriteBatch batch, TextureAtlas atlas) {
        for (Tile t : tiles) {
            t.render(batch, atlas);
        }
    }

    public void initTextures(TextureAtlas atlas) {
        for (Tile t : tiles) {
            t.initTextures(atlas);
        }
    }

    public boolean collides(Rectangle rect) {
        boolean hasCollision = false;
        wallDestroyed = false;
        for (Tile t : tiles) {
            if (t.isCollidable() && t.collides(rect)) {
                hasCollision = true;
                wallDestroyed = t.isDead();
            }
        }
        return hasCollision;
    }

    public boolean collides(Circle circle) {
        boolean hasCollision = false;
        wallDestroyed = false;
        for (Tile t : tiles) {
            if (t.isCollidable() && t.collides(circle)) {
                hasCollision = true;
                wallDestroyed = t.isDead();
            }
        }
        return hasCollision;
    }
}
