package com.silvano.wallbreaker.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Locale;

/**
 * Created by rodrigo.camara on 02/01/2017.
 */

public class LanguageUtils {

    public static I18NBundle getLanguageBundle(){
        FileHandle baseFileHandle = Gdx.files.internal("strings/strings");
        Locale locale = java.util.Locale.getDefault();
        I18NBundle languageBundle = I18NBundle.createBundle(baseFileHandle, locale);
        return languageBundle;
    }
}
