package com.silvano.wallbreaker;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.silvano.wallbreaker.screen.LoadingScreen;

public class WallbreakerGame extends Game {

    private SpriteBatch batch;
    private ShapeRenderer shape;
    private AssetManager manager;

    public SpriteBatch getBatch() {
        return batch;
    }

    public ShapeRenderer getShape() {
        return shape;
    }

    public AssetManager getManager() {
        return manager;
    }

    @Override
    public void create () {
	    batch = new SpriteBatch();
	    shape = new ShapeRenderer();
        manager = new AssetManager();
        setScreen(new LoadingScreen(this));
    }

    @Override
    public void render () {
	super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        manager.clear();
        manager.dispose();
        shape.dispose();
        batch.dispose();
    }
}
